const Discord = require('discord.js');
var rn = require('random-number');
const bot = new Discord.Client();
require('dotenv').config()
const PREFIX = process.env.PREFIX
const TOKEN = process.env.TOKEN
const MongoClient = require('mongodb').MongoClient;
const uri = "mongodb+srv://projetnodejs:projetnodejs@cluster0.eq0dh.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });


bot.on('ready', () => {
  console.log(`Logged in as ${bot.user.tag}!`);
  client.connect(err => {
    //const collection = client.db("test").collection("devices");
      console.log("Database connected")
      client.close();
  });
});

bot.on('message', msg => {

  let args = msg.content.split(' ');

  if(msg.content === PREFIX + "help") {
    const embed = new Discord.MessageEmbed()
    .setTitle(`**HELP**`)
    .setColor(0x000000)
    .addFields(
      { name: `**${PREFIX}help**`, value: `Affiche la liste des commandes.`},
      { name: `**${PREFIX}d <faces> <dés>**`, value: `Exécute un lancé de dé (si aucun argument n'est rentré, 1d6 sera lancé).`},
    )
    msg.channel.send(embed);
    }

  if (msg.content.startsWith(PREFIX + "d")) {
    
    console.log(args[1])
    console.log(args[2])
    
    let faces, dices

    if(args[1] === undefined) {
      faces = 6
    } else {
      faces = args[1]
    }

    if(args[2] === undefined) {
      dices = 1 
    } else {
      dices = args[2]
    }

    let nbFaces = {
      min: 1,
      max: faces,
      integer: true
    }
    msg.channel.send(`${dices}d${faces} lancé(s)`)

    for (let index = 1; index <= dices; index++) {
      let nb = rn(nbFaces)
      msg.channel.send(`dé ${index} : ${nb}`)
    } 
  }

  if (msg.content === PREFIX + "list") {
      msg.channel.send("Liste")
  }

  if (msg.content === PREFIX + "create-char") {
    msg.channel.send("creation")
    }

    if (msg.content === PREFIX + "update-char") {
        msg.channel.send("modification")
    }

    if (msg.content === PREFIX + "delete-char") {
        msg.channel.send("suppression")
    }

    if (msg.content === PREFIX + "create") {
        msg.channel.send("creation stat")
    }



});

bot.login(TOKEN);