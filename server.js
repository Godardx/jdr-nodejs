const { count } = require('console');

const path = require('path');
const express = require('express');
const app = express();
const http = require('http').createServer(app);
const io = require('socket.io')(http)

let dir = path.join(__dirname, 'public');

app.use(express.static(dir));

const MongoClient = require('mongodb').MongoClient;
const uri = "mongodb+srv://projetnodejs:projetnodejs@cluster0.eq0dh.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });

client.connect((err, db) => {
  if (err) {
    console.log("error")
    process.exit(1);
  } else {
    let dbo = db.db('projetNodeJs-JDR');
    console.log("Bdd connected")
    let connectedUsers = 0;

    let stockedMessages = [];

    app.get('/', (req, res) => {
      res.sendFile(__dirname + '/index.html');
    });

    app.get('/master', (req, res) => {
      res.sendFile(__dirname + '/master.html');
    });

    app.get('/player1', (req, res) => {
      res.sendFile(__dirname + '/player-1.html');
    });

    app.get('/player2', (req, res) => {
      res.sendFile(__dirname + '/player-2.html');
    });


    http.listen(3000, () => {
        console.log('Server ready');
      });

    io.on ('connection', (socket) => {
        console.log('Utilisateur connecté')
        io.emit('chat message', `Un utilisateur s'est connecté`);


        dbo.collection("equipement").find({}, { projection: { _id: 0 } }).toArray(function(err, result) {
          if (err) throw err;
          for (let i = 0; i < result.length; i++) {
            //stockedMessages.push(result[i].message);
            console.log(i)
            console.log(result)
            socket.emit(`player 1 equip`, result);
          }
        })

        dbo.collection("user").find({}, { projection: { _id: 0 } }).toArray(function(err, result) {
          if (err) throw err;
          for (let i = 0; i < result.length; i++) {
            //stockedMessages.push(result[i].message);
            console.log(i)
            console.log(result)
            socket.emit(`player 1 info`, result);
          }
        })

        dbo.collection("messages").find({}, { projection: { _id: 0 } }).toArray(function(err, result) {
          if (err) throw err;
          for (let i = 0; i < result.length; i++) {
            //stockedMessages.push(result[i].message);
            console.log(result.length);
            console.log(result[i].message);
            socket.emit('stocked messages', result[i].message);
          }
          
        })
        console.log(stockedMessages);
        connectedUsers++;


        socket.on('disconnect', () => {
            console.log('Utilisateur deconnecté');
            io.emit('chat message', `Un utilisateur s'est déconnecté`);
            connectedUsers--;
            countUsers(connectedUsers);

          });
          countUsers(connectedUsers);

          socket.on('chat message', (msg) => {
            io.emit('chat message', msg);
            let obj = {message: msg}
            dbo.collection('messages').insertOne(obj, function(err, res) {
              if (err) throw err;
              console.log("Message inserted")
            })
          });

          function countUsers(users) {
            
            io.emit('connectedUsers', connectedUsers)

            console.log(`Utilisateurs en ligne : ${connectedUsers}`)

          }


          
    })
    let objectTemplate = { nom: "Name", description: "Description", type: "Type" };
    let userTemplate = { nom: "Bensmaine", prenom: "Quentin", bio: "Oeoe", age: 19, classe:"Il est là", equipement: "Equipement", stats: "Statistiques", Objets: "Objets", argent: 1500 };
    let equipmentTemplate = { player_name: "player-2", casque: true, gants: false, plastron: true, pantalon: true, chaussures: true, main_gauche: false, main_droite: false };
    let statsTemplate = { player_name: "player-1", force: 5, agilite: 3, furtivite: 4, intelligence: 3, res_physique: 3, res_magique: 2, hp: 100, mp: 100, xp: 40, lvl: 10 };

    // dbo.collection('objects').insertOne(objectTemplate, function(err, res) {
    //   if (err) throw err;
    //   console.log("Template Object inserted")
    // })

    // dbo.collection('user').insertOne(userTemplate, function(err, res) {
    //   if (err) throw err;
    //   console.log("Template User inserted")
    // })

    // dbo.collection('equipement').insertOne(equipmentTemplate, function(err, res) {
    //   if (err) throw err;
    //   console.log("Equipment User inserted")
    // })

    // dbo.collection('stats').insertOne(statsTemplate, function(err, res) {
    //   if (err) throw err;
    //   console.log("Stats User inserted")
    // })

    

  }
})
