# JDR NodeJS

## Brief :

TP qui consiste à faire une interface semblable à celle d'un JDR !

---

## Sujet :

- Créer un Front le plus simpliste possible avec une interface de chat + affichage des fiches de chaques joueurs (et non joueurs si le MJ rend la fiche visible).
- Créer un serveur REST avec Fastify et Mongoose + héberger la BDD sur MongoAtlas.
- Il doit y avoir une route pour le MJ qui lui permettra de voir toutes les fiches et les modifier directrement depuis ces dernières sans passer par des commandes dans le chat textuel.
- Créer des commandes pour :
  - Lister les fiches personnages.
  - Créer une fiche personnage.
  - Modifier une fiche personnage.
  - Supprimer une fiche personnage
  - Créer une (ou plusieurs) fiche avec les stats limité **[Ex : !create 3 Squelettes 6]**

---

## Exemple pour les pages 

### Page web vue -> Joueur

![Screenshot](/img/image-20210510161337860.png?raw=true "Vue Joueur")



### Page web vue -> MJ

![Screenshot](/img/image-20210510161514468.png?raw=true "Vue MJ")

---

## Technologies utilisées

- HTML / CSS / JS
- Fastify
- Mongoose
- MongoDB Atlas

---

## Installation :

- Etape 1 : Cloner le projet
- Etape 2 : Lancer le projet dans un IDE et faire `node server.js`
- Etape 3 : Lancer un navigateur et aller à l'adresse suivante : `localhos:3000`

---

### Crédit :

**BENSMAINE Quentin**

**MACE Corentin**

**GODARD Thomas**

---
